let fs = require("fs");
let path = require("path");
const { isBuffer } = require("util");

function problem2(){
fs.readFile("../lipsum.txt", "utf-8", function(err, data){
    if(err){
        console.error(err)
        return;
    }else{
        console.log(data);
    }
    let upper = data.toUpperCase();
    fs.writeFile("./file1.txt", upper, function(err){
        if(err){
            return console.error(err)
        }
        fs.writeFile("./filesName.txt", "file1.txt", function(err){
            if(err){
                return console.error(err)
            }
            fs.readFile("./file1.txt", "utf-8", function(err, upperData){
                if(err){
                    console.error(err);
                    return;
                }else{
                    console.log(upperData);
                }
                let lowered = upperData.toLowerCase();
                let splitted = lowered.split(".");
                let newFile = fs.createWriteStream("file2.txt");
                newFile.on("error", function(err){
                    console.error(err);
                })
                splitted.forEach(element => newFile.write(`${element}\r\n`));
                newFile.end();
                fs.appendFile("./filesName.txt", " file2.txt", function(err){
                    if(err){
                        console.error(err);
                    }else{
                        console.log("New File added in Filesname");
                    }
                    fs.readFile("./file2.txt", "utf-8", function(err, sortArr){
                        if(err){
                            console.error(err);
                            return;
                        }else{
                            console.log(sortArr);
                        }
                        sortArr = Array.of(sortArr);
                        // console.log(sortArr);
                        let sortedArray = sortArr.sort();
                        // console.log(sortedArray);
                        sortedArray = sortedArray.toString();
                        fs.writeFile("./file3.txt", sortedArray, function(err){
                            if(err){
                                console.error(err);
                                return;
                            }else{
                                console.log("New File created");
                            }
                            fs.appendFile("./filesName.txt", " file3.txt", function(err){
                                if(err){
                                    console.error(err);
                                }
                                fs.readFile("./filesName.txt", "utf-8", function(err, names){
                                    if(err){
                                        console.error(err);
                                    }else{
                                        console.log(names);
                                    }
                                    let splitFilesToDelete = names.split(" ");
                                    let deletedFiles = splitFilesToDelete.map((file) => {
                                        fs.unlink(file, function(err){
                                            if(err){
                                                console.error(err);
                                            }else{
                                                console.log("Files Deleted");
                                            }
                                        })
                                    })
                                })
                            })
                        })

                    })
                })
                    
            })
        })
    })
    
    });
}

module.exports = problem2;
    
    

 